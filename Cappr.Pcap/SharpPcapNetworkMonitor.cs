﻿using Cappr.Core;
using Cappr.Core.Net;
using PacketDotNet;
using SharpPcap;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cappr.Pcap
{
    public class SharpPcapNetworkMonitor : INetworkMonitor
    {
        UserConfiguration _config;

        public event EventHandler<NetworkEvent> NetworkEventOccurred;

        public SharpPcapNetworkMonitor(UserConfiguration config)
        {
            _config = config ?? throw new ArgumentNullException(nameof(config));
        }

        public void Start()
        {
            var device = GetDevice();
            device.OnPacketArrival += OnPacketArrival;
            device.Open(DeviceMode.Promiscuous, 1000);
            device.Filter = "ether host " + string.Join(" or ", _config.MacActionIdMappings.Keys.Select(FormatMacAddress));
            device.StartCapture();
        }

        static string FormatMacAddress(string macAddress)
            => new string(FormatMacAccress(macAddress.ToArray()).ToArray());

        static IEnumerable<char> FormatMacAccress(char[] macAddress)
        {
            for (var i = 0; i < macAddress.Length; i++)
            {
                yield return char.ToLowerInvariant(macAddress[i]);
                if (i < macAddress.Length - 1 && i % 2 == 1)
                {
                    yield return ':';
                }
            }
        }

        ICaptureDevice GetDevice()
        {
            var deviceNames = new HashSet<string>();
            var devices = CaptureDeviceList.Instance.ToArray();
            for (var i = 0; i < devices.Length; i++)
            {
                var deviceName = string.Join(":", devices[i].ToString()
                    .Split('\n')
                    .FirstOrDefault(l => l.Contains("FriendlyName"))
                    .Split(":")
                    .Skip(1)).Trim();
                deviceNames.Add(deviceName);

                if (_config.DeviceNames.Contains(deviceName))
                {
                    Console.WriteLine($"Found device {deviceName}");
                    return devices[i];
                }
            }

            throw new Exception($"Could not find any devices from the config. Available devices: {string.Join(",", deviceNames)}");
        }

        void OnPacketArrival(object sender, CaptureEventArgs e)
        {
            var networkEvent = new NetworkEvent
            {
                EventType = NetworkEventType.Other
            };

            if (e.Packet.LinkLayerType == LinkLayers.Ethernet)
            {
                var ethernetPacket = Packet.ParsePacket(e.Packet.LinkLayerType, e.Packet.Data) as EthernetPacket;
                networkEvent.SourceMacAddress = ethernetPacket.SourceHwAddress.ToString();
                if (ethernetPacket.Extract(typeof(ARPPacket)) is ARPPacket arpPacket)
                {
                    networkEvent.EventType = NetworkEventType.DeviceJoined;
                }
            }

            NetworkEventOccurred?.Invoke(this, networkEvent);
        }
    }
}