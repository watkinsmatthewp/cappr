# cappr

## What is Cappr?

This is a C# network sniffer with the ability to customize automation rules for handling new ARP probe packets while persisting a black and whitelist of MAC addresses.

## Wait, but why?

I got a bunch of Amazon Dash Buttons on Prime day for $1 each and I want to hack them to automate whatever I want :)

So basically, this is a .NET port of Ted Benson's [article on hacking the Amazon Dash Button](https://blog.cloudstitch.com/how-i-hacked-amazon-s-5-wifi-button-to-track-baby-data-794214b0bdd8). But I don't want to install Python. And I want to slap a nice management UI on top of it so I can easily add and manage which Dash Buttons do which actions without having to change my code.

Ultimately I want to get this deplyed to my Raspberry Pi and get this off my laptop, so I chose .NET Core.