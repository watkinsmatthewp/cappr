﻿using Cappr.Core;
using Cappr.Core.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Cappr.ConsoleApp
{
    public class JsonFileDataRepository : IDataRepository
    {
        readonly string _directoryPath;
        const string MAC_ADDRESSES_FILE_NAME = "mac_addresses.json";
        readonly Lazy<Dictionary<string, ListEntry>> _macAddressRegistrationCache;
        const string USER_CONFIGURATION_FILE_NAME = "user.json";

        public JsonFileDataRepository(string directoryPath)
        {
            _directoryPath = directoryPath ?? throw new ArgumentNullException(directoryPath);
            _macAddressRegistrationCache = new Lazy<Dictionary<string, ListEntry>>(ReadFromDisk<Dictionary<string, ListEntry>>(MAC_ADDRESSES_FILE_NAME) ?? new Dictionary<string, ListEntry>());
        }

        public ListEntry LookupMacAddressRegistration(string macAddress) => _macAddressRegistrationCache.Value.ContainsKey(macAddress)
            ? _macAddressRegistrationCache.Value[macAddress]
            : new ListEntry { Status = ListEntryStatus.None };

        public void UpdateMacAddressRegistration(string macAddress, ListEntry listEntry)
        {
            _macAddressRegistrationCache.Value[macAddress] = listEntry;
            SaveToDisk(MAC_ADDRESSES_FILE_NAME, _macAddressRegistrationCache.Value);
        }

        public void DeleteMacAddressRegistration(string macAddress)
        {
            if (_macAddressRegistrationCache.Value.ContainsKey(macAddress))
            {
                _macAddressRegistrationCache.Value.Remove(macAddress);
                SaveToDisk(MAC_ADDRESSES_FILE_NAME, _macAddressRegistrationCache.Value);
            }
        }

        public UserConfiguration LookupUserConfiguration()
            => ReadFromDisk<UserConfiguration>(USER_CONFIGURATION_FILE_NAME);

        public void UpdateUserConfiguration(UserConfiguration userConfiguration)
            => SaveToDisk(USER_CONFIGURATION_FILE_NAME, userConfiguration);

        #region Private helpers

        T ReadFromDisk<T>(string fileName)
        {
            var filePath = Path.Combine(_directoryPath, fileName);
            return File.Exists(filePath)
                ? JsonConvert.DeserializeObject<T>(File.ReadAllText(filePath))
                : default(T);
        }

        void SaveToDisk<T>(string fileName, T obj)
        {
            var filePath = Path.Combine(_directoryPath, fileName);
            File.WriteAllText(filePath, JsonConvert.SerializeObject(obj, Formatting.Indented));
        }

        #endregion Private helpers
    }
}