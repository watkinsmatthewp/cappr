﻿using Cappr.Core;
using Cappr.Core.Net;
using Cappr.Core.Rules;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Cappr.ConsoleApp
{
    public class WebhookNetworkEventRule : NetworkEventRule
    {
        static readonly HttpClient _client = new HttpClient();
        static Dictionary<string, DateTime> LAST_TRIGGERED = new Dictionary<string, DateTime>();

        UserConfiguration _userConfiguration;
        TimeSpan _threshold;

        public WebhookNetworkEventRule(UserConfiguration userConfiguration)
        {
            _userConfiguration = userConfiguration ?? throw new ArgumentNullException();
            _threshold = TimeSpan.FromSeconds(userConfiguration.ThresholdSeconds);
        }

        public override void Execute(NetworkEvent networkEvent)
        {
            if (LAST_TRIGGERED.ContainsKey(networkEvent.SourceMacAddress))
            {
                var timeSinceLastTrigger = DateTime.UtcNow - LAST_TRIGGERED[networkEvent.SourceMacAddress];
                if (timeSinceLastTrigger < _threshold)
                {
                    return;
                }
            }

            LAST_TRIGGERED[networkEvent.SourceMacAddress] = DateTime.UtcNow;
            Console.WriteLine($"Got a/n {networkEvent.EventType} event from {networkEvent.SourceMacAddress}");

            if (!_userConfiguration.MacActionIdMappings.ContainsKey(networkEvent.SourceMacAddress))
            {
                Console.WriteLine($"No mapping in the user configuration for mac {networkEvent.SourceMacAddress}");
                return;
            }

            var action = _userConfiguration.MacActionIdMappings[networkEvent.SourceMacAddress];
            Console.WriteLine($"Executing action {action}");

            var url = string.Format(_userConfiguration.PostUrlFormat, action);
            var result = _client.PostAsync(url, new StringContent(string.Empty)).GetAwaiter().GetResult();
            result.EnsureSuccessStatusCode();

            Console.WriteLine($"Executed action {action}");
        }
    }
}