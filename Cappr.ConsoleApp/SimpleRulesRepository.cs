﻿using Cappr.Core.Rules;
using System.Collections.Generic;

namespace Cappr.ConsoleApp
{
    public class SimpleRulesRepository : INetworkEventRuleRepository
    {
        public List<INetworkEventRule> Rules { get; private set; } = new List<INetworkEventRule>();
        public IEnumerable<INetworkEventRule> GetAllRules() => Rules;
    }
}