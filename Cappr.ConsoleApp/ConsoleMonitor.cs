﻿using Cappr.Core;
using Cappr.Core.Data;
using Cappr.Core.Net;
using Cappr.Core.Rules;
using System;

namespace Cappr.ConsoleApp
{
    public class ConsoleMonitor : CapprMonitor
    {
        public ConsoleMonitor(IDataRepository data, INetworkEventRuleRepository rules, INetworkMonitor networkMonitor)
            : base(data, rules, networkMonitor)
        {
        }

        protected override bool IgnoreNewDevice(string macAddress)
        {
            Console.WriteLine($"Ignoring new device {macAddress}");
            return true;
        }
    }
}