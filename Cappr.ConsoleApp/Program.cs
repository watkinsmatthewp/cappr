﻿using Cappr.Core;
using Cappr.Core.Data;
using Cappr.Core.Net;
using Cappr.Pcap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Cappr.ConsoleApp
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Welcome");
                Run(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static void Run(string[] args)
        {
            Console.WriteLine("Initializing data repository.");
            var configDirectoryPath = args.Last();
            if (!Directory.Exists(configDirectoryPath))
            {
                Directory.CreateDirectory(configDirectoryPath);
            }
            var data = new JsonFileDataRepository(Path.Combine(configDirectoryPath));

            Console.WriteLine("Getting configuration information");
            var config = data.LookupUserConfiguration();
            if (config == null)
            {
                config = new UserConfiguration();
                Console.WriteLine("Action URL format?");
                config.PostUrlFormat = Console.ReadLine();
                Console.WriteLine("Device name?");
                config.DeviceNames.Add(Console.ReadLine());
                data.UpdateUserConfiguration(config);
            }

            foreach (var action in config.MacActionIdMappings)
            {
                Console.WriteLine($"FYI, MAC {action.Key} currently maps to action {action.Value}");
            }

            // TODO: Allow pass in
            //while (true)
            //{
            //    Console.WriteLine("Add new trigger MAC Address (or \"done\")?");
            //    var macAddress = Console.ReadLine();
            //    if (macAddress.ToLowerInvariant() == "done")
            //    {
            //        break;
            //    }
            //    Console.WriteLine("Action ID?");
            //    userConfiguration.MacActionIdMappings[macAddress] = Console.ReadLine();
            //    data.UpdateMacAddressRegistration(macAddress, new ListEntry
            //    {
            //        RegisteredOnUTC = DateTime.UtcNow,
            //        Status = ListEntryStatus.Important
            //    });
            //}

            Console.WriteLine("Starting network monitor");
            var network = new SharpPcapNetworkMonitor(config);
            var rulesRepository = new SimpleRulesRepository();
            rulesRepository.Rules.Add(new WebhookNetworkEventRule(config));
            var monitor = new ConsoleMonitor(data, rulesRepository, network);

            monitor.Start();
        }
    }
}