﻿using Cappr.Core.Net;
using System.Collections.Generic;

namespace Cappr.Core.Rules
{
    public abstract class NetworkEventRule : INetworkEventRule
    {
        public HashSet<NetworkEventType> PacketEventTypes { get; set; }
        public HashSet<string> SourceMacAddresses { get; set; }

        public bool Matches(NetworkEvent networkEvent)
            => PacketEventTypes?.Contains(networkEvent.EventType) != false
            && SourceMacAddresses?.Contains(networkEvent.SourceMacAddress) != false;

        public abstract void Execute(NetworkEvent networkEvent);
    }
}