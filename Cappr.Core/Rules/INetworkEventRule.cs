﻿using Cappr.Core.Net;

namespace Cappr.Core.Rules
{
    public interface INetworkEventRule
    {
        bool Matches(NetworkEvent networkEvent);
        void Execute(NetworkEvent networkEvent);
    }
}