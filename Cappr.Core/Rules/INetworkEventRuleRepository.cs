﻿using System.Collections.Generic;

namespace Cappr.Core.Rules
{
    public interface INetworkEventRuleRepository
    {
        IEnumerable<INetworkEventRule> GetAllRules();
    }
}