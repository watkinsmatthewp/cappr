﻿using Cappr.Core.Data;
using Cappr.Core.Net;
using Cappr.Core.Rules;
using System;
using System.Linq;

namespace Cappr.Core
{
    public abstract class CapprMonitor
    {
        public IDataRepository Data { get; private set; }
        public INetworkEventRuleRepository Rules { get; private set; }
        public INetworkMonitor NetworkMonitor { get; private set; }

        public CapprMonitor(IDataRepository data, INetworkEventRuleRepository rules, INetworkMonitor networkMonitor)
        {
            Data = data ?? throw new ArgumentNullException(nameof(data));
            Rules = rules ?? throw new ArgumentNullException(nameof(rules));
            NetworkMonitor = networkMonitor ?? throw new ArgumentNullException(nameof(networkMonitor));
            NetworkMonitor.NetworkEventOccurred += OnNetworkEventOccurred;
        }

        public void Start() => NetworkMonitor.Start();

        protected abstract bool IgnoreNewDevice(string macAddress);

        #region Private helpers

        void OnNetworkEventOccurred(object sender, NetworkEvent networkEvent)
        {
            var listEntry = Data.LookupMacAddressRegistration(networkEvent.SourceMacAddress);
            if (listEntry.Status == ListEntryStatus.None)
            {
                listEntry.Status = IgnoreNewDevice(networkEvent.SourceMacAddress) ? ListEntryStatus.Ignore : ListEntryStatus.Important;
                listEntry.RegisteredOnUTC = DateTime.UtcNow;
                Data.UpdateMacAddressRegistration(networkEvent.SourceMacAddress, listEntry);
            }

            if (listEntry.Status == ListEntryStatus.Important)
            {
                var dominantRule = Rules.GetAllRules().FirstOrDefault(r => r.Matches(networkEvent));
                dominantRule?.Execute(networkEvent);
            }
        }

        #endregion Private helpers
    }
}