﻿using System;

namespace Cappr.Core.Data
{
    public class ListEntry
    {
        public ListEntryStatus Status { get; set; }
        public DateTime RegisteredOnUTC { get; set; }
    }
}