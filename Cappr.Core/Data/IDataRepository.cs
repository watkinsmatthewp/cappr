﻿namespace Cappr.Core.Data
{
    public interface IDataRepository
    {
        ListEntry LookupMacAddressRegistration(string macAddress);
        void UpdateMacAddressRegistration(string macAddress, ListEntry listEntry);
        void DeleteMacAddressRegistration(string macAddress);
        UserConfiguration LookupUserConfiguration();
        void UpdateUserConfiguration(UserConfiguration userConfiguration);
    }
}