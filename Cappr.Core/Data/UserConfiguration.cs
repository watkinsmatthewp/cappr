﻿using System.Collections.Generic;

namespace Cappr.Core
{
    public class UserConfiguration
    {
        public int ThresholdSeconds { get; set; } = 10;
        public string PostUrlFormat { get; set; }
        public List<string> DeviceNames { get; set; } = new List<string>();
        public Dictionary<string, string> MacActionIdMappings { get; set; } = new Dictionary<string, string>();
    }
}
