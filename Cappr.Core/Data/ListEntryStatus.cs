﻿namespace Cappr.Core.Data
{
    public enum ListEntryStatus
    {
        None = 0,
        Ignore = 100,
        Important = 200
    }
}