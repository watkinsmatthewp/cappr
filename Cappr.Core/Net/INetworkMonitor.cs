﻿using System;

namespace Cappr.Core.Net
{
    public interface INetworkMonitor
    {
        event EventHandler<NetworkEvent> NetworkEventOccurred;

        void Start();
    }
}