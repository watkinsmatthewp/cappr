﻿namespace Cappr.Core.Net
{
    public class NetworkEvent
    {
        public NetworkEventType EventType { get; set; }
        public string SourceMacAddress { get; set; }
    }
}